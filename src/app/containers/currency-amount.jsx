import React from 'react';
import {bindActionCreators} from 'redux';
import _ from 'lodash';
import {newCurrencyAmount} from '../actions/new-currency-amount';
import {connect} from 'react-redux';
  
class CurrencyAmount extends React.Component {

  newAmount(val, that) {
    var n = val.target.value

    if (!isNaN(parseFloat(n)) && isFinite(n)) {
      // all good
    }
    else {
      n = n.replace(/\D/g,'') // remove letters and special characters apart from .
    }

    var sign = that.props.type == 'to' ? '+' : '-'
    var amount = _.replace(n, sign, '') // remove the sign
    if (parseFloat(amount) || amount == 0) { // check if amount is a valid number
      that.props.newCurrencyAmount({amount: amount, type: that.props.type})
    }
    else
      console.log('invalid')
  }

  render() {
    var {rates, type} = this.props
    var amount = rates[type+'Val'] || ''
    if (amount != '') {
      var sign = type == 'to' ? '+' : '-'
      amount = sign+amount
    }

    return <div>
      <input
        className="currency-amount"
        name={type+"amount"} onChange={(val) => {this.newAmount(val, this)}}
        value={amount}
      />
    </div>
  }
}

function mapStateToProps(state) {
  return {
    rates: state.rates
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({newCurrencyAmount: newCurrencyAmount}, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(CurrencyAmount)

import React from 'react';
import {bindActionCreators} from 'redux';
import _ from 'lodash';
import {transferMoney} from '../actions/transfer-money';
import {connect} from 'react-redux';
  
class ExchangeButton extends React.Component {

  transferMoney(that) {
    var {rates, type} = that.props

    // do not allow transfers if balance will be negative
    // or both currencies are the same
    if (!rates.negativeBal && rates.fromCurr != rates.toCurr)
      this.props.transferMoney({rates:rates, type: type})
  }

  render() {
    var exchangeClassName = this.props.rates.negativeBal ? 'disable-exchange' : ''

    return <div id="exchange" className={exchangeClassName} onClick={() => {this.transferMoney(this)}}>
      Exchange
    </div>
  }
}

function mapStateToProps(state) {
  return {
    rates: state.rates
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({transferMoney: transferMoney}, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(ExchangeButton)

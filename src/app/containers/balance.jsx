import React from 'react';
import {bindActionCreators} from 'redux';
import {updateBalance} from '../actions/update-balance';
import {connect} from 'react-redux';
  
class Balance extends React.Component {

  currSymbol(rates, type) {
    // console.log('type',type)
    var symbol = ''
    switch (rates[type+'Curr']) {
      case 'gbp': {
        symbol = '£'
        break;
      }
      case 'eur': {
        symbol = '€'
        break;
      }
      case 'usd': {
        symbol = '$'
        break;
      }
    }

    return symbol
  }

  render() {
    var {rates, type} = this.props
    var currSymbol = this.currSymbol(rates, type)
    var balanceClassName = rates.negativeBal && type == 'from' ? 'unsufficient-balance' : ''
    var balance = rates[rates[type+'Curr']+'Bal']
    return <div className="balance">
      <p className={balanceClassName}>You have {currSymbol}{balance}</p>
    </div>
  }
}

function mapStateToProps(state) {
  return {
    rates: state.rates
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({updateBalance: updateBalance}, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(Balance)

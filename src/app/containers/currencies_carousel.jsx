import React from 'react';
import Slider from 'react-slick';
import {changeCurrencies} from '../actions/change-currencies';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
  
class CurrenciesCarousel extends React.Component {

  change(tab, that) {
    var currency = ''

    switch(tab) {
      case 0:
        currency = 'gbp'
        break;
      case 1:
        currency = 'eur'
        break;
      case 2:
        currency = 'usd'
        break;
    }

    this.props.changeCurrencies({type: that.props.screen, currency: currency})
  }

  render() {

    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };

    var {defaultTab} = this.props
    return (
      <Slider {...settings}
        afterChange={(val) => {this.change(val, this)}}
        initialSlide={defaultTab}
        arrows={false}>
        <div className="currency">GBP</div>
        <div className="currency">EUR</div>
        <div className="currency">USD</div>
      </Slider>
    )
  }
}

function mapStateToProps(state) {
  return {
    rates: state.rates
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({changeCurrencies: changeCurrencies}, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(CurrenciesCarousel)

import React from 'react';
import {bindActionCreators} from 'redux';
import {updateRates} from '../actions/update-rates';
import {connect} from 'react-redux';
  
class CurrentRate extends React.Component {

  componentDidMount() {
    this.props.updateRates() // init
    setInterval(this.props.updateRates, 30000) // refresh every 30 seconds
  }

  exchangeRate(rates, type, otherType) {
    var {eur, usd, gbp} = rates

    if (usd == 0)
      return 0
    else {
      return  rates[rates[otherType+'Curr']] / rates[rates[type+'Curr']]
    }
  }

  displayRate() {
    var {rates, type, top} = this.props
    var {eur, usd, gbp, fromCurr, toCurr} = rates

    if (fromCurr != toCurr)
    {
      var currSymbols = {
        eur: '€',
        gbp: '£',
        usd: '$'
      }
      var otherType = type == 'to' ? 'from' : 'to'
      var decimalPoints = top ? 4 : 2
      var exchange = this.exchangeRate(rates, type, otherType).toFixed(decimalPoints)
      return currSymbols[rates[type+'Curr']]+'1 = '+currSymbols[rates[otherType+'Curr']] + exchange
      
    }
    return "Rates"
  }

  render() {
    var {rates, type, top} = this.props
    var displayRate = this.displayRate()
    var rateClassName = top ? '' : 'balance-rate'
    return <div className={rateClassName}>{displayRate}</div>
  }
}

function mapStateToProps(state) {
  return {
    rates: state.rates
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({updateRates: updateRates}, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(CurrentRate)

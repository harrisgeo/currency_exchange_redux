export const transferMoney = (rates) => {
  return {
    type: 'MONEY_TRANSFER',
    payload: rates
  }
}

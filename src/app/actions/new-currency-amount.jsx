export const newCurrencyAmount = (val) => {
  return {
    type: 'NEW_CURRENCY_AMOUNT',
    payload: val
  }
}

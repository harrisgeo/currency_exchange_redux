import request from 'superagent'

export const updateRates = (rates) => {
  var req = request.get('http://api.fixer.io/latest')
  return {
    type: 'RATE_UPDATE',
    payload: req
  }
}

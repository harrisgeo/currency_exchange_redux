export const changeCurrencies = (currencies) => {
  return {
    type: 'CHANGE_CURRENCY',
    payload: currencies
  }
}

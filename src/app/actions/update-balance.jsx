export const updateBalance = (rates) => {
  return {
    type: 'BALANCE_UPDATE',
    payload: rates
  }
}

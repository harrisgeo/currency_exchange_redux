import {createStore, combineReducers, applyMiddleware} from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import promise from "redux-promise-middleware";
import request from 'superagent';
import {createLogger} from 'redux-logger';
import rates from './reducers/reducer-rates';

const reducers = combineReducers({
  rates
})

const store = createStore(
    reducers,
    {},
    applyMiddleware(
      // createLogger(), // uncomment for debugging
      thunk,
      promise()
    )
);

export default store

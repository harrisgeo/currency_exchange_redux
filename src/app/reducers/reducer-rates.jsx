let data = {
  baseCurr: 'eur',
  eur: 0,
  eurBal: 12,
  gbp: 0,
  gbpBal: 10,
  negativeBal: false,
  usd: 0,
  usdBal: 13,
  fromCurr: 'gbp',
  toCurr: 'eur',
  fromVal: 0,
  toVal: 0
}

export default (state=data, action) => {
  switch (action.type) {
    case "RATE_UPDATE": { // promise start
      return {...state}
      break;
    }
    case "RATE_UPDATE_PENDING": { // promise pending
      return {...state}
      break;
    }
    case "RATE_UPDATE_FULFILLED": { // promise finished
      var rates = action.payload.body.rates
      state.eur = 1
      state.usd = rates.USD
      state.gbp = rates.GBP

      return {...state, state}
      break;
    }
    case "CHANGE_CURRENCY": { // select new current
      var {type, currency} = action.payload
      state[type+'Curr'] = action.payload.currency

      return {...state, state}
      break;
    }
    case "NEW_CURRENCY_AMOUNT": { // apply currency exchange
      // console.log('nca', action)
      var {type, amount} = action.payload

      var other = type == 'to' ? 'from' : 'to' // first amount comes from "from" then other is "to" and vice versa
      var firstCurr = type+'Curr' // get the first currency depending on the input
      var otherCurr = other+'Curr' // get the other currency
      var firstVal = type+'Val' // get the first value depending on the input
      var otherVal = other+'Val' // get the other value

      var stateFirstCurr = state[firstCurr] // get the currency rates of the first currency
      var stateOtherCurr = state[otherCurr] // get the currency rates of the other currency

      var stateOtherVal = (parseFloat(state[stateOtherCurr]) / parseFloat(state[stateFirstCurr])) * amount // exchange rate amount
      state[firstVal] = amount
      state[otherVal] = amount != 0 ? stateOtherVal.toFixed(2) : '' // give the other value the correct exchange rate amount
      state.negativeBal = state[state[firstCurr]+'Bal'] - amount < 0 ? true : false // if input amount higher than balance, show negative
      return {...state, state}
      break;
    }
    case "MONEY_TRANSFER": {
      var {type, rates} = action.payload
      var otherType = type == 'to' ? 'from' : 'to'

      var otherBal = parseFloat(state[state[otherType+'Curr']+'Bal'])
      var newBalance = otherBal + parseFloat(rates[otherType+'Val'])

      state[state[type+'Curr']+'Bal'] -= parseFloat(rates[type+'Val'])
      state[state[otherType+'Curr']+'Bal'] = newBalance.toFixed(2)
      state[type+'Val'] = 0
      state[otherType+'Val'] = 0
      return {...state, state}
      break;
    }
  }
  return state
}

import React from 'react';
import CurrentRate from '../../containers/current-rate';

export default class CurrencyFrom extends React.Component {

  render() {
    return <div className="rates-outer">
      <div className="rates-inner">
        <CurrentRate type="from" top={true}/>
      </div>
    </div>
  }
}

import React from 'react';
import Currencies from '../../containers/currencies_carousel';
import Balance from '../../containers/balance';
import CurrencyAmount from '../../containers/currency-amount';
import CurrentRate from '../../containers/current-rate'

export default class CurrencyFrom extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    var {defaultTab, type} = this.props
    var arrow = type == 'to' ? 'arrow-down' : ''
    return <div className={"currency-"+type} >
      <div className="currency-amount-div">
        <div className={arrow}></div>
        <Currencies screen={type} defaultTab={defaultTab}/>
        <Balance type={type}/>
        <CurrencyAmount type={type}/>
        <CurrentRate type={type} top={false}/>
      </div>
    </div>
  }
}

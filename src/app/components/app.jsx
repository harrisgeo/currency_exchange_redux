import React from 'react';
import Rate from './currencies/rates-box';
import Currency from './currencies/currencies-group';
import Exchange from '../containers/exchange-button'
require('../style.less');

export default class App extends React.Component {
  render() {
    return <div>
      <Rate type="from"/>
      <Currency type="from" defaultTab={0}/>
      <Currency type="to" defaultTab={1}/>
      <Exchange type="from"/>
    </div>
  }
}

